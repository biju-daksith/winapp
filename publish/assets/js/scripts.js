
//Global Variables

var appName = "Orion Lite";
var appURL = "https://sggiornqas.saint-gobain-glass.com/OrionLite/";
var userName = "";

window.onload = function () {

    var screenWidth = this.window.innerWidth;

    /*
        Screen Width is more than 1366 means Extra Large displays
    */
    if (parseInt(screenWidth) > 1366) {

        $('.redirect-container .content-footer').css('top', '468px');
        $('.redirect-container .msg-text').css({
            'font-size': '42px'
        });

        $('.redirect-container .msg-caption').css({
            'margin-top': '130px',
            'font-size': '26px'
        });

        $('.redirect-container .welcome-msg p').css({
            'font-size': '22px'
        });

        $('.redirect-container .redirect-text').css({
            'margin-top': '94px',
            'font-size': '20px'
        });
    }
};

$(document).ready(function () {

    $(".content-footer").find("h1").text(appURL);
    $(".redirect-container .redirect-text p a").attr("href", appURL);
    $(".welcome-msg p b").text(userName);

    $("#CountDownTimer").TimeCircles({
        total_duration: 30,
        time: {
            Days: {
                show: false
            },
            Hours: {
                show: false
            },
            Minutes: {
                show: false
            },
            Seconds: {
                color: "#fff700"
            }
        },
        circle_bg_color: "#98cddc",
        count_past_zero: false
    }).addListener(countDownComplete);

    function countDownComplete(unit, value, total) {
        /*
            param total is count down tick
        */
        if (total == 0) {
            //redirect here
            window.location.href = appURL;
        }
    }
});

$(".startTimer").click(function () {
    $("#CountDownTimer").TimeCircles().start();
});

$(".stopTimer").click(function () {
    $("#CountDownTimer").TimeCircles().stop();
});

$(".download-shortcut").click(function () {
    window.location = "http://sconnecthub.saint-gobain-glass.com/utility/api/download/ShortCut?LinkName=" + appName + "&LinkURL=" + appURL;
    //    window.location = "http://sconnecthub.saint-gobain-glass.com/utility/api/download/ShortCut?LinkName=MyInfoV2&LinkURL=http://sggiapps.saint-gobain-glass.com/myinfoV2";
});

$(".redirect-url").click(function () {
    window.location.href = appURL;
});





/*




*/
