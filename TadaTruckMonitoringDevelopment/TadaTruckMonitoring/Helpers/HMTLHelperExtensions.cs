﻿using TadaTruckMonitoring.Models;
using TadaTruckMonitoring.Entity;
using TadaTruckMonitoring.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Data;
using System.Web.Script.Serialization;
using SystemWebHttpContext = System.Web.HttpContext;
using System.Configuration;
using System.Data.SqlClient;

namespace TadaTruckMonitoring
{

    public static class HMTLHelperExtensions
    {
       
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null, string cssClass = null)
        {

            if (String.IsNullOrEmpty(cssClass)) 
                cssClass = "active";

            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return controller == currentController && action == currentAction ?
                cssClass : String.Empty;
        }

        public static string PageClass(this HtmlHelper html)
        {
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            return currentAction;
        }
        
        //public static string GETUSERNAME(this HtmlHelper html)
        //{
        //    //var session = SystemWebHttpContext.Current.Session;
        //    string dd = HttpContext.Current.Session["FULL_NAME"].ToString();
        //    System.Diagnostics.Trace.WriteLine(dd);
        //    return dd;
        //}

        public static string GETUSERNAME(this HtmlHelper html)
        {
            try
            {
                if (HttpContext.Current.Session["LOGIN_STAT"] == "SUCCESS")
                {

                    //   return;
                    string dd = HttpContext.Current.Session["FULL_NAME"].ToString();
                    System.Diagnostics.Trace.WriteLine(dd);
                    return dd;
                }
                else
                {
                    //    HttpContext.Current.Session["GLOBAL_CONN"] = ConfigurationManager.ConnectionStrings["INITConnection"].ConnectionString;

                    //  return RedirectToAction("Login", "Login");
                }
                return "";
            }
            catch
            {
                return "";
            }

            
        }

        public static string getjsondata(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> Rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Row = null;
            foreach (DataRow dr in dt.Rows)
            {
                Row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Row.Add(col.ColumnName.Trim(), dr[col]);
                }
                Rows.Add(Row);
            }
            return jsSerializer.Serialize(Rows);
        }

        public static string Gettranscationcode(this HtmlHelper html)
        {
            string JsonData = string.Empty;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cm = new SqlCommand("select C_APP_PROP from XT_XCON where LT_FLAG is not null", con);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            string jsonTBL1 = getjsondata(dt);
            jsonTBL1 = jsonTBL1.Replace("&quot;", "");
            jsonTBL1 = jsonTBL1.Replace(@"\", "");
            return jsonTBL1;
        }
        public static string GETMENU(this HtmlHelper html)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<li class=\"\">");
            sb.Append(Environment.NewLine);
            sb.Append("<a href=\"\">");
            sb.Append("Internal Movements of ID Sheets");
            sb.Append("</a>");
            sb.Append(Environment.NewLine);
            sb.Append("</li>");

            //string return_data = "<li class=""><a href="">Internal Movements of ID Sheets</a></li>";
            return sb.ToString();
         
            
        }


	}
}
