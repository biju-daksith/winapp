﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Configuration;
using TadaTruckMonitoring.Models;
using System.Text;
using TadaTruckMonitoring.Entity;
using TadaTruckMonitoring.Control;

namespace TadaTruckMonitoring.Controllers
{
    [SessionTimeout]
    public class DashboardsController : Controller
    {
        User_Entity _userentity = new User_Entity();
        User_Model _usermodel = new User_Model();
        public ActionResult Home()
        {

                    //Session["FULL_NAME"] = "";
                    //Session["USER_NAME"] = "";
                    //Session["LANG"] = "";
                    //Session["SGID"] = "";
                    //Session["C_PARM"] = "";
                    //Session["SITE"] = "";
                    //Session["FORM"] = "";
                    //Session["APP"] = "";
                    //Session["GROUP"] = "";
                    //Session["CURR_LANG"] =  "";
                    //Session["GLOBAL_CONN"] = "";

            if (Session["LOGIN_STAT"] != "SUCCESS")
            {
                Session["GLOBAL_CONN"] = ConfigurationManager.ConnectionStrings["INITConnection"].ConnectionString;
                // Globals.SetGlobalConnection(ConfigurationManager.ConnectionStrings["INITConnection"].ConnectionString);
                return this.RedirectToAction("Login", "Login");
                //   return;
            }
            return View();

            //DataSet dsMFG = new DataSet();
            //dsMFG = _usermodel.getLoadMainMenus(Session["LANG"].ToString(), Session["APP"].ToString(), Session["GROUP"].ToString(), "", Session["USER_NAME"].ToString());
            //return View(dsMFG);
            //ViewBag.Count = new List<string>()
            //{
            //    "1",
            //    "2"
            //};

          //  return View();
        }

        public ActionResult Menu_home()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<li class=\"\">");
            sb.Append(Environment.NewLine);
            sb.Append("<a href=\"\">");
            sb.Append("Internal Movements of ID Sheets");
            sb.Append("</a>");
            sb.Append(Environment.NewLine);
            sb.Append("</li>");
            return View(sb.ToString());
        }
  

        public ActionResult MVTS()
        {
            return View();
        }

        public ActionResult Dashboard_2()
        {
            return View();
        }

        public ActionResult Dashboard_3()
        {
            return View();
        }

        public ActionResult Dashboard_4()
        {
            return View();
        }

        public ActionResult Dashboard_4_1()
        {
            return View();
        }

        public ActionResult Dashboard_5()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GET_FICH_DETAILS(string flag, string fich_no, string bay_rack, string lang, string userid, string c_racklevel, string da_no)
        {

            string JsonData = string.Empty;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cm = new SqlCommand("SP_LITE_WEB_MVTS", con);
            cm.CommandType = CommandType.StoredProcedure;

            cm.Parameters.AddWithValue("@FLAG", flag);
            cm.Parameters.AddWithValue("@FICH", fich_no);
            cm.Parameters.AddWithValue("@BAYRACK", bay_rack);
            cm.Parameters.AddWithValue("@LANG", lang);
            cm.Parameters.AddWithValue("@USERID", userid);
            cm.Parameters.AddWithValue("@C_RACKLEVEL", c_racklevel);
            cm.Parameters.AddWithValue("@DA_NO", da_no);

            SqlDataAdapter da = new SqlDataAdapter(cm);

            DataSet ds = new DataSet();
            da.Fill(ds);
            string jsonTBL1 = getjsondata(ds.Tables[0]);
            var _LstHolding = new { jsonTBL1 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet); 
        }

        [HttpGet]
        public JsonResult SAVE_FICH_DETAILS(string flag, string fich_no, string bay_rack, string lang, string userid, int c_racklevel, string da_no, string txmode, string t_device, string halfframe, string idoc_req)
        {
            string JsonData = string.Empty;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            SqlCommand cm = new SqlCommand("SP_LITE_WEB_MVTS_SAVE", con);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.AddWithValue("@FLAG", flag);
            cm.Parameters.AddWithValue("@FICHE", fich_no);
            cm.Parameters.AddWithValue("@BAYRACK", bay_rack);
            cm.Parameters.AddWithValue("@LANG", lang);
            cm.Parameters.AddWithValue("@USERID", userid);
            cm.Parameters.AddWithValue("@C_RACKLEVEL", c_racklevel);
            cm.Parameters.AddWithValue("@DA", da_no);
            cm.Parameters.AddWithValue("@TXMODE", txmode);
            cm.Parameters.AddWithValue("@T_DEVICE", t_device);
            cm.Parameters.AddWithValue("@HALFFRAME", halfframe);
            cm.Parameters.AddWithValue("@IDOC_XREQ", idoc_req);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string jsonTBL2 = getjsondata(ds.Tables[0]);

            
            //JsonData = getjsondata(dt);

            var _LstHolding2 = new { jsonTBL2 };
            return Json(_LstHolding2, JsonRequestBehavior.AllowGet); 
        }

        
        public string FICH_STATUS_DETAILS(string strRequestNO)
        {
          //  string returnedval = "json retured";
          //  string returnedval2 = "json retured";

            string JsonData = string.Empty;


            Console.WriteLine(strRequestNO.ToString());
            DataTable dt = new DataTable();
            dt.Columns.Add("Status");
            dt.Columns.Add("value");

            DataRow _val = dt.NewRow();
            _val["Status"] = "success";
            _val["value"] = "1";
            dt.Rows.Add(_val);


            JsonData = getjsondata(dt);
            return JsonData;
                              
        }

        public string getjsondata(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> Rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Row = null;
            foreach (DataRow dr in dt.Rows)
            {
                Row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Row.Add(col.ColumnName.Trim(), dr[col]);
                }
                Rows.Add(Row);
            }
            return jsSerializer.Serialize(Rows);
        }

        public string FORMSEARCH(string text)
        {
            try
            {
                SqlConnection con = null;

                if (Session["GLOBAL_CONN"] == " " || Session["GLOBAL_CONN"] == null)
                {
                    con = new SqlConnection(ConfigurationManager.ConnectionStrings["INITConnection"].ConnectionString);

                }
                else
                {
                    con = new SqlConnection(Session["GLOBAL_CONN"].ToString());
                }
                string result = string.Empty;
                string lang = Session["LANG"].ToString();
                string user = Session["SGID"].ToString();
                SqlCommand cmd = new SqlCommand("select count(*) from xt_xcon where C_CON='" + text + "'", con);
                con.Open();
                int count = Convert.ToInt16(cmd.ExecuteScalar());
                con.Close();
                if (count == 1)
                {
                    SqlCommand cm = new SqlCommand("SP_LITE_LOAD_MENUSUBMENUNEW", con);
                    cm.CommandType = CommandType.StoredProcedure;
                    cm.Parameters.AddWithValue("@LANGUAGE", lang);
                    cm.Parameters.AddWithValue("@APPLICATION", "LG");
                    cm.Parameters.AddWithValue("@USERGROUP", Session["GROUP"].ToString());
                    cm.Parameters.AddWithValue("@MENUGROUP", "");
                    cm.Parameters.AddWithValue("@USERID", user);

                    SqlDataAdapter da = new SqlDataAdapter(cm);
                    DataSet dt = new DataSet();
                    da.Fill(dt);
                    if (dt.Tables[1].Rows.Count > 0)
                    {
                        for (int i = 0; i < dt.Tables[1].Rows.Count; i++)
                        {
                            DataRow dr = dt.Tables[1].Rows[i];
                            if (dr["C_CON"].ToString() == text)
                            {
                                return (dr["LT_ACTION"].ToString() + "," + dr["LT_CONTROL"].ToString());
                            }
                        }
                    }
                    return ("InternalServerError" + "," + "Pages");
                }
                else
                {
                    return ",";
                }
            }
            catch
            {
                return ",";
            }
        }

        public string Menu()
        {
            SqlConnection con = null;
            if (Session["GLOBAL_CONN"] == " " || Session["GLOBAL_CONN"] == null)
            {
                con = new SqlConnection(ConfigurationManager.ConnectionStrings["INITConnection"].ConnectionString);
            }
            else
            {
                con = new SqlConnection(Session["GLOBAL_CONN"].ToString());
            }
            string result = string.Empty;
            string lang = Session["LANG"].ToString();
            string user = Session["SGID"].ToString();
            SqlCommand cm = new SqlCommand("SP_LITE_LOAD_MENUSUBMENUNEW", con);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.AddWithValue("@LANGUAGE", lang);
            cm.Parameters.AddWithValue("@APPLICATION", "LG");
            cm.Parameters.AddWithValue("@USERGROUP", Session["GROUP"].ToString());
            cm.Parameters.AddWithValue("@MENUGROUP", "");
            cm.Parameters.AddWithValue("@USERID", user);

            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataSet dt = new DataSet();
            da.Fill(dt);
            for(int i=0;i<dt.Tables[1].Rows.Count;i++)
            {
                if (i == 0)
                {
                    result = dt.Tables[1].Rows[i].ItemArray[9].ToString();
                }
                else
                {
                    result += ","+dt.Tables[1].Rows[i].ItemArray[9].ToString();
                }
            }
            return result;
            //string jsonTBL2 = getjsondata(dt.Tables[1]);
            //var _LstHolding2 = new { jsonTBL2 };
            //return Json(_LstHolding2, JsonRequestBehavior.AllowGet); 
        }
        //public string GET_FICH_DETAILS(string flag, string fich_no, string bay_rack, string lang, string userid, string c_racklevel, string da_no)
        //{

        //    string JsonData = string.Empty;




        //    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        //    SqlCommand cm = new SqlCommand("SP_LITE_WEB_MVTS", con);
        //    cm.CommandType = CommandType.StoredProcedure;

        //    cm.Parameters.AddWithValue("@FLAG", flag);
        //    cm.Parameters.AddWithValue("@FICH", fich_no);
        //    cm.Parameters.AddWithValue("@BAYRACK", bay_rack);
        //    cm.Parameters.AddWithValue("@LANG", lang);
        //    cm.Parameters.AddWithValue("@USERID", userid);
        //    cm.Parameters.AddWithValue("@C_RACKLEVEL", c_racklevel);
        //    cm.Parameters.AddWithValue("@DA_NO", da_no);

        //    SqlDataAdapter da = new SqlDataAdapter(cm);
        //    DataTable dt = new DataTable();
        //    da.Fill(dt);


        //    JsonData = getjsondata(dt);
        //    return JsonData;

        //}
    }
}