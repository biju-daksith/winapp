﻿using TadaTruckMonitoring.Models;
using TadaTruckMonitoring.Entity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using SystemWebHttpContext = System.Web.HttpContext;
using System.IO;
using System.Net;
using System.Xml;

namespace TadaTruckMonitoring.Controllers
{
    public class LoginController : Controller
    {
        MessageValidationModel _alertmodel = new MessageValidationModel();
        MessageValidationEntity _alertentity = new MessageValidationEntity();
        User_Entity _userentity = new User_Entity();
        User_Model _usermodel = new User_Model();
        public string[] SGID;
        public string idoclinedata, fileLoc, connection, langlocprocess, encryptconn, decryptconn, filename, currlang;
        public ActionResult Login()
        {            
            if (Request.ServerVariables["SERVER_PORT"] == "80")
            {
                return RedirectToAction("TruckStatus", "TruckMonitoring"); 
            }
            else
            {
                return RedirectToAction("TruckStatus", "TruckMonitoring");
            }   
         
            Session["GLOBAL_CONN"] = ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString;
            return View();
        }
        public ActionResult ChangePassword(string old_pwd, string new_pwd, string conf_pwd)
        {
            try
            {
                err_msg = "";
                DataTable dtMINL = _usermodel.getMasterData(Session["SITE"].ToString(), "PMINL");
                DataTable dtMAXL = _usermodel.getMasterData(Session["SITE"].ToString(), "PMAXL");
                //Regex regPassword = new Regex(@"^(?=.*[0-9])(?=.*[a-zA-Z])\w{8,}$", RegexOptions.IgnorePatternWhitespace);
                Regex regPassword = new Regex(@"^((?=.*\d)(?=.*[a-z])((?=.*\W)|(?=.*_)))", RegexOptions.IgnorePatternWhitespace);
                if (old_pwd.Trim() == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A538", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Enter Old Password", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtoldpassword.Focus();
                    return Content(err_msg);
                }
                if (new_pwd.Trim() == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A539", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    // MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // txtnewpassword.Focus();
                    // return;
                }
                else if (new_pwd.Trim().Length < Convert.ToInt32(dtMINL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A540", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(string.Format(_alertentity.alertmessage, dtMINL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtnewpassword.Focus();
                    //return;
                }
                else if (new_pwd.Trim().Length > Convert.ToInt32(dtMAXL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A550", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(string.Format(_alertentity.alertmessage, dtMAXL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtnewpassword.Focus();
                    //return;
                }
                else if (regPassword.IsMatch(new_pwd.Trim()) == false)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A541", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtnewpassword.Focus();
                    //return;
                }
                if (conf_pwd.Trim() == "")
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A542", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtconfirmpassword.Focus();
                    //return;
                }
                else if (conf_pwd.Trim().Length < Convert.ToInt32(dtMINL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A543", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(string.Format(_alertentity.alertmessage, dtMINL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtconfirmpassword.Focus();
                    //return;
                }
                else if (conf_pwd.Trim().Length > Convert.ToInt32(dtMAXL.Rows[0]["NEWVALUE"].ToString()))
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A551", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(string.Format(_alertentity.alertmessage, dtMAXL.Rows[0]["NEWVALUE"].ToString()), _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtconfirmpassword.Focus();
                    //return;
                }
                else if (regPassword.IsMatch(conf_pwd.Trim()) == false)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A544", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //MessageBox.Show("Confirm password must be composed of a combination of at least three of the following character types: letters, numbers, and special characters.", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    // txtconfirmpassword.Focus();
                    //return;
                }

                //SGID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Split('\\');
                _userentity = _usermodel.getValidateLoginUser(Session["USER_NAME"].ToString(), Encryptdata(old_pwd.Trim()));

                if (_userentity.username != "" && _userentity.username != null)
                {
                    if (new_pwd.Trim() == conf_pwd.Trim())
                    {
                        DataTable dt2 = _usermodel.getPasswordHistory(Session["USER_NAME"].ToString(), Encryptdata(new_pwd.Trim()));
                        if (dt2.Rows.Count > 0)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A545", Session["LANG"].ToString());
                            err_msg = _alertentity.alertmessage;
                            return Content(err_msg);
                            //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            ////MessageBox.Show("You have already used this password. Please give different password.", "Warning Message", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            //txtnewpassword.Focus();
                            //return;
                        }
                        _userentity.flag = _usermodel.getUpdateNewPwd(Session["USER_NAME"].ToString(), Encryptdata(new_pwd.Trim()));

                        if (_userentity.flag == true)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A546", Session["LANG"].ToString());
                            err_msg = _alertentity.alertmessage;
                            Session["USER_LOGIN"] = "SUCCESS";
                            //return this.RedirectToAction("Dashboards", "Dashboard_1");
                            return Content(err_msg);
                            //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //txtoldpassword.Text = "";
                            //txtnewpassword.Text = "";
                            //txtconfirmpassword.Text = "";
                            //return;
                        }
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A547", Session["LANG"].ToString());
                        err_msg = _alertentity.alertmessage;
                        return Content(err_msg);
                        //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //txtnewpassword.Focus();
                        //return;
                    }
                }
                else
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A548", Session["LANG"].ToString());
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg);
                    //MessageBox.Show(_alertentity.alertmessage, _alertentity.alerttitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //txtoldpassword.Focus();
                    //return;
                }


            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                err_msg = ex.Message.ToString();

            }
            return Content(err_msg);
        }


        public ActionResult LOG_OUT_BTN(string flag)
        {
            //_userentity.flag = _usermodel.getDeleteLoginUsers(Session["USER_NAME"].ToString(), Session["SGID"].ToString());
            Session["GLOBAL_CONN"] = (ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString);
            Session.Abandon();
            return this.RedirectToAction("Login", "Login");
            // return Content("redirect");

        }
        [HttpGet]
        public string Error_Message(string code)
        {

            _alertmodel = new MessageValidationModel();
            _alertentity = _alertmodel.Alert_Information("LG", code, "E");
            return _alertentity.alertmessage;
        }

        public string Error_Message_INIT(string code, string init)
        {

            _alertmodel = new MessageValidationModel();
            _alertentity = _alertmodel.Alert_Information("LG", code, "E");
            return _alertentity.alertmessage;
        }

        public JsonResult LOGIN_DETAILS(string flag, string site)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString);
            SqlCommand cm = new SqlCommand("SP_LOADLANGLOCAT", con);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.AddWithValue("@STATUS", "LOAD");
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string jsonTBL0 = getjsondata(ds.Tables[0]);
            string jsonTBL1 = getjsondata(ds.Tables[1]);
            var _LstHolding2 = new { jsonTBL0, jsonTBL1 };
            return Json(_LstHolding2, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DB_CONN(string flag_, string site_, string lang_)
        {
            Session["CURR_LANG"] = lang_;
            Session["LANG"] = lang_;
            System.Diagnostics.Trace.WriteLine(flag_);
            System.Diagnostics.Trace.WriteLine(site_);
            System.Diagnostics.Trace.WriteLine(lang_);
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString);
            SqlCommand cm = new SqlCommand("SP_LOADLANGLOCAT", con);
            cm.CommandType = CommandType.StoredProcedure;
            cm.Parameters.AddWithValue("@STATUS", flag_);
            cm.Parameters.AddWithValue("@SITE", site_);
            SqlDataAdapter da = new SqlDataAdapter(cm);
            DataSet ds = new DataSet();
            da.Fill(ds);
            string dec_before = ds.Tables[0].Rows[0].ItemArray[1].ToString();
            string dec = Decryptdata(ds.Tables[0].Rows[0].ItemArray[1].ToString());
            Session["GLOBAL_CONN"] = dec;
            if (Session["GLOBAL_CONN"] != "")
            {
                System.Diagnostics.Trace.WriteLine(Session["GLOBAL_CONN"]);
            }
            System.Diagnostics.Trace.WriteLine(Session["GLOBAL_CONN"]);
            System.Diagnostics.Trace.WriteLine(dec);
            System.Diagnostics.Trace.WriteLine(dec_before);
            return Content("");
        }
        string err_msg = "";

        public ActionResult VALIDATE_UN_PW(string auth, string uname_, string password_, string plant_, string lang_)
        {

            if (auth == "orn_clicked")
            {
                if (uname_ != "" && password_ != "")
                {
                    _userentity = _usermodel.getValidateLoginUserOnly(uname_, Encryptdata(password_));
                }

                if (_userentity.username != null)
                {

                    _userentity.flag = _usermodel.getUpdateWrongPwd(uname_, Encryptdata(password_));

                    if (_userentity.flag == true)
                    {
                        _userentity = _usermodel.getValidateLoginUserOnly(uname_);
                        if (_userentity.wrngpassattmpt < _userentity.wrngpasslimit)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A102", lang_);
                            err_msg = string.Format(_alertentity.alertmessage, _userentity.wrngpasslimit - _userentity.wrngpassattmpt);
                            return Content(err_msg + "---" + "A102");
                        }

                        if (_userentity.wrngpassattmpt == _userentity.wrngpasslimit || _userentity.wrngpassattmpt > _userentity.wrngpasslimit)
                        {
                            _userentity.flag = _usermodel.getAccountLock(uname_);
                            if (_userentity.flag == true)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A103", lang_);
                                err_msg = _alertentity.alertmessage;
                                return Content(err_msg + "---" + "A103");
                            }
                        }
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A104", lang_);
                        err_msg = _alertentity.alertmessage;
                        return Content(err_msg + "---" + "A104");
                    }
                }

                if (uname_ != "" && password_ != "")
                {
                    _userentity = _usermodel.getValidateLoginUser(uname_, Encryptdata(password_));
                }

                if (_userentity.username != null && _userentity.loginstatus == 1)
                {
                    SGID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Split('\\');

                    currlang = _usermodel.getCurrLnag(lang_);
                    Session["FULL_NAME"] = _userentity.fullName.ToUpper();
                    System.Diagnostics.Trace.WriteLine(Session["FULL_NAME"].ToString());
                    ViewBag.USERNAME = Session["FULL_NAME"];
                    Session["USER_NAME"] = _userentity.username.ToUpper();
                    Session["LANG"] = lang_;
                    Session["SGID"] = _userentity.username.ToUpper();
                    Session["C_PARM"] = _userentity.cparam;
                    Session["SITE"] = _userentity.site;
                    Session["FORM"] = _userentity.form;
                    Session["APP"] = _userentity.application;
                    Session["GROUP"] = _userentity.group;
                    Session["CURR_LANG"] = currlang;

                    DataTable dtLock = _usermodel.getMultiUserLoginLock();
                    if (dtLock.Rows.Count > 0)
                    {
                        if (dtLock.Rows[0][0].ToString() == "Y")
                        {

                            _userentity = _usermodel.getValidateMultiUserLogin(uname_);

                            if (_userentity.sgid != null)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A105", lang_);
                                err_msg = _alertentity.alertmessage;
                                return Content(err_msg + "---" + "A105");
                            }
                            else
                            {
                                fun_loadform(uname_, password_);
                            }
                        }
                        else
                        {
                            fun_loadform(uname_, password_);
                        }
                    }

                }
                else if (_userentity.loginstatus == 0)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A106", lang_);
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg + "---" + "A106");
                }
                else if (_userentity.loginstatus == 1)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A107", lang_);
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg + "---" + "A107");
                }
                else
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A108", lang_);
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg + "---" + "A108");
                }


            }
            if (auth == "win_clicked")
            {

                string Uri = "http://sggiorndev.saint-gobain-glass.com/orionlitewindowslogin/ADLogin.asmx/WindowLogon";
                string Data_WITH_PW = Service_Windows_Login(Uri, uname_, "", password_);
                if (Data_WITH_PW.Contains("false"))
                {
                    _userentity.flag = _usermodel.getUpdateWrongPwd(uname_, Encryptdata(password_));

                    if (_userentity.flag == true)
                    {
                        _userentity = _usermodel.getValidateLoginUserOnly(uname_);
                        if (_userentity.wrngpassattmpt < _userentity.wrngpasslimit)
                        {
                            _alertentity = _alertmodel.Alert_Information("LG", "A102", lang_);
                            err_msg = string.Format(_alertentity.alertmessage, _userentity.wrngpasslimit - _userentity.wrngpassattmpt);
                            return Content(err_msg + "---" + "A102");
                        }

                        if (_userentity.wrngpassattmpt == _userentity.wrngpasslimit || _userentity.wrngpassattmpt > _userentity.wrngpasslimit)
                        {
                            _userentity.flag = _usermodel.getAccountLock(uname_);
                            if (_userentity.flag == true)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A103", lang_);
                                err_msg = _alertentity.alertmessage;
                                return Content(err_msg + "---" + "A103");
                            }
                        }
                    }
                    else
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "A104", lang_);
                        err_msg = _alertentity.alertmessage;
                        return Content(err_msg + "---" + "A104");
                    }
                }

                if (Data_WITH_PW.Contains("true"))
                {
                    _userentity = _usermodel.getValidateLoginUser(uname_, Encryptdata(password_));
                }

                if (_userentity.username != null && _userentity.loginstatus == 1)
                {
                    SGID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString().Split('\\');
                    currlang = _usermodel.getCurrLnag(lang_);
                    Session["FULL_NAME"] = _userentity.fullName.ToUpper();
                    System.Diagnostics.Trace.WriteLine(Session["FULL_NAME"].ToString());
                    Session["USER_NAME"] = _userentity.username.ToUpper();
                    Session["LANG"] = lang_;
                    Session["SGID"] = _userentity.username.ToUpper();
                    Session["C_PARM"] = _userentity.cparam;
                    Session["SITE"] = _userentity.site;
                    Session["FORM"] = _userentity.form;
                    Session["APP"] = _userentity.application;
                    Session["GROUP"] = _userentity.group;
                    Session["CURR_LANG"] = currlang;

                    DataTable dtLock = _usermodel.getMultiUserLoginLock();
                    if (dtLock.Rows.Count > 0)
                    {
                        if (dtLock.Rows[0][0].ToString() == "Y")
                        {
                            _userentity = _usermodel.getValidateMultiUserLogin(uname_);
                            if (_userentity.sgid != null)
                            {
                                _alertentity = _alertmodel.Alert_Information("LG", "A105", lang_);
                                err_msg = _alertentity.alertmessage;
                                return Content(err_msg + "---" + "A105");
                            }
                            else
                            {
                                fun_loadform(uname_, password_);
                            }
                        }
                        else
                        {
                            fun_loadform(uname_, password_);

                        }
                    }

                }
                else if (_userentity.loginstatus == 0)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A106", lang_);
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg + "---" + "A106");

                }
                else if (_userentity.loginstatus == 1)
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A107", lang_);
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg + "---" + "A107");
                }
                else
                {
                    _alertentity = _alertmodel.Alert_Information("LG", "A108", lang_);
                    err_msg = _alertentity.alertmessage;
                    return Content(err_msg + "---" + "A108");
                }

            }

            if (err_msg == "")
            {
                Session["LOGIN_STAT"] = "SUCCESS";

            }

            return Content(err_msg);
        }

        public string Service_Windows_Login(string uri, string userid, string domain, string password)
        {
            string json = string.Empty;
            string Link = string.Empty;
            DataSet ds = new DataSet();
            System.Net.WebClient wc = new System.Net.WebClient();

            Link = "http://sggiorndev.saint-gobain-glass.com/orionlitewindowslogin/ADLogin.asmx/WindowLogon";
            var URI = new Uri(Link);
            wc.QueryString.Add("userId", userid);
            wc.QueryString.Add("domain", domain);
            wc.QueryString.Add("password", password);

            var data = wc.UploadValues(URI, "POST", wc.QueryString);
            var responseString = UnicodeEncoding.UTF8.GetString(data);

            return responseString;

        }
        public ActionResult fun_loadform(string username, string password)
        {

            DataTable dt = _usermodel.getMasterData(Session["SITE"].ToString(), "DP");
            DataTable dt1 = _usermodel.getMasterData(Session["SITE"].ToString(), "PTL");
            DataTable dt2 = _usermodel.getPasswordChangeTime(username.Trim(), Encryptdata(password.Trim()));


            _userentity.flag = _usermodel.getInsertLoginUsers(username.ToUpper(), Session["USER_NAME"].ToString(), Environment.MachineName.ToString(), System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());

            if (_userentity.flag == true)
            {

                if (dt.Rows.Count > 0)
                {
                    if (password.ToUpper() == dt.Rows[0]["NEWVALUE"].ToString().ToUpper())
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "B015", "E");
                        err_msg = _alertentity.alertmessage;
                    }
                }
                if (dt1.Rows.Count > 0 && dt2.Rows.Count > 0)
                {
                    if (Convert.ToInt32(dt1.Rows[0]["NEWVALUE"].ToString()) < Convert.ToInt32(dt2.Rows[0]["TDAYS"].ToString()))
                    {
                        _alertentity = _alertmodel.Alert_Information("LG", "B016", "E");
                        err_msg = _alertentity.alertmessage;
                    }
                }

            }

            return Content(err_msg);
        }


        public string getjsondata(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> Rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Row = null;
            foreach (DataRow dr in dt.Rows)
            {
                Row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Row.Add(col.ColumnName.Trim(), dr[col]);
                }
                Rows.Add(Row);
            }
            return jsSerializer.Serialize(Rows);
        }


        private string Encryptdata(string password)
        {
            string strmsg = string.Empty;
            byte[] encode = new byte[password.Length];
            encode = Encoding.UTF8.GetBytes(password);
            strmsg = Convert.ToBase64String(encode);
            return strmsg;
        }

        private string Decryptdata(string encryptpwd)
        {
            string decryptpwd = string.Empty;
            UTF8Encoding encodepwd = new UTF8Encoding();
            Decoder Decode = encodepwd.GetDecoder();
            byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
            int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
            char[] decoded_char = new char[charCount];
            Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
            decryptpwd = new String(decoded_char);
            return decryptpwd;
        }

        public ActionResult Login1()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
            ViewBag.HeaderTitle = "Login";

            string UserName = string.Empty;
            DataTable DTClientMap = new DataTable();
            DataTable DTClientName = new DataTable();

            string CASHOST = ConfigurationManager.AppSettings["CASHOST"].ToString();
            // Look for the "ticket=" after the "?" in the URL
            string tkt = Request.QueryString["ticket"];

            // This page is the CAS service=, but discard any query string residue
            string service = Request.Url.GetLeftPart(UriPartial.Path);

            string AbsoluteUrl = service.Substring(0, service.LastIndexOf('/') + 1);

            // First time through there is no ticket=, so redirect to CAS login
            if (tkt == null || tkt.Length == 0)
            {
                //string redir = CASHOST + "login?" + "service=" + service;
                string redir = CASHOST + "login?ticket=" + tkt + "&" + "service=" + AbsoluteUrl + ConfigurationManager.AppSettings["ValidatePage"].ToString();
                Response.Redirect(redir);
                return View();
            }
            else
            {
                // Second time (back from CAS) there is a ticket= to validate
                string validateurl = CASHOST + "validate?" + "ticket=" + tkt + "&" + "service=" + AbsoluteUrl + ConfigurationManager.AppSettings["ValidatePage"].ToString();
                StreamReader Reader = new StreamReader(new WebClient().OpenRead(validateurl));
                string resp = Reader.ReadToEnd();
                // I like to have the text in memory for debugging rather than parsing the stream

                // Some boilerplate to set up the parse.
                NameTable nt = new NameTable();
                XmlNamespaceManager nsmgr = new XmlNamespaceManager(nt);
                XmlParserContext context = new XmlParserContext(null, nsmgr, null, XmlSpace.None);
                XmlTextReader reader = new XmlTextReader(resp, XmlNodeType.Element, context);


                string[] arrUID = new string[20];
                arrUID = resp.ToString().Split('\n');
                //Session["UserSGID"] = arrUID[1].ToString();

                Session["UserSGID"] = arrUID[1];


                string SGIDSSO = Session["UserSGID"].ToString();


                //ADMINEUREntities drop = new Models.ADMINEUREntities();
                SqlConnection con = null;

                if (Session["GLOBAL_CONN"] == " " || Session["GLOBAL_CONN"] == null)
                {
                    con = new SqlConnection(ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString);

                }
                else
                {
                    con = new SqlConnection(Session["GLOBAL_CONN"].ToString());
                }

                SqlCommand cmd = new SqlCommand("SP_ADMIN_CONSOLE_WEB_SSO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UNAME", SGIDSSO);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    reader.Close();
                    Session["LOGIN_STAT"] = "SUCCESS";
                    Session["FULL_NAME"] = dr["FULLNAME"].ToString();
                    System.Diagnostics.Trace.WriteLine(Session["FULL_NAME"].ToString());
                    ViewBag.USERNAME = Session["FULL_NAME"];
                    Session["USER_NAME"] = dr["FULLNAME"].ToString();
                    Session["SGID"] = SGIDSSO;
                    Session["C_PARM"] = dr["C_PARM"].ToString();
                    Session["SITE"] = dr["C_LIEU"].ToString();
                    Session["FORM"] = dr["TY_OUTIL"].ToString();
                    Session["APP"] = dr["C_APP"].ToString();
                    Session["GROUP"] = dr["C_UTI"].ToString();
                    TempData["notice"] = "";
                    return RedirectToAction("OrderStatus", "OrderStatusDashboard");
                }
                else
                {
                    Session["failure"] = "failure";
                    return RedirectToAction("Login", "Login");

                }
            }


        }
    }
}