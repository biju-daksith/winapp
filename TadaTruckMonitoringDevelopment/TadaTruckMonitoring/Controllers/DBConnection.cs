﻿using TadaTruckMonitoring.Models;
using TadaTruckMonitoring.Entity;
using TadaTruckMonitoring.Controllers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.IO;
namespace TadaTruckMonitoring.Model
{

    public class DBConnection
    {
        private SqlDataAdapter myAdapter;
        private SqlConnection conn;
        User_Entity _userentity = new User_Entity();

        //private int status;
        /// <constructor>
        /// Initialise Connection
        /// </constructor>
        public DBConnection()
        {
            myAdapter = new SqlDataAdapter();
            if ((HttpContext.Current.Session["GLOBAL_CONN"] == "") || (HttpContext.Current.Session["GLOBAL_CONN"] == null))
            {
                conn = new SqlConnection(ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString);
            }
            else
            {
                conn = new SqlConnection(HttpContext.Current.Session["GLOBAL_CONN"].ToString());
            }
            //string conname = ConfigurationSettings.AppSettings["SGGIDBConnection"].ToString();
            //conn = new SqlConnection(conname);
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        public SqlConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                // MessageBox.Show(e.Message, "Error Message - A179");
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dataTable;
        }

        /// <method>
        /// Select Query
        /// </method>
        public DataTable executeSelectQueryWithSPNew(String _storedprocedure, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _storedprocedure;
                myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                // MessageBox.Show(e.Message, "Error Message - A179");
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dataTable;
        }

        public DataSet executeSelectQueryNew(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                //ds1 = ds.Tables[0];
                //ds1.Tables.Add(dataTable);
            }
            catch (SqlException e)
            {
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                //  MessageBox.Show(e.Message, "Error Message - A179");
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return ds;
        }

        public DataSet executeSelectQueryWithSP(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            try
            {
                myCommand.CommandTimeout = 0;
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                //ds1 = ds.Tables[0];
                //ds1.Tables.Add(dataTable);
            }
            catch (SqlException e)
            {
                // MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return ds;
        }
        /// <summary>
        /// For report . Returning adapter 
        /// </summary>
        /// <param name="_query"></param>
        /// <param name="sqlParameter"></param>
        /// <returns></returns>
        public SqlDataAdapter executeSelectQueryWithSPreturnsSqlDataAdapter(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                return myAdapter;
                //ds1 = ds.Tables[0];
                //ds1.Tables.Add(dataTable);
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            //return ds;
        }
        /// <method>
        /// Method to Return Data Table -- Added by Shankar.M January 05,2012 04:13:00 PM
        /// </method>
        public DataTable executeSelectQuerySPDataTable(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myAdapter.InsertCommand = myCommand;
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(dt);
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt;
        }

        /// <method>
        /// Method to Return Data Reader -- Added by Shankar.M June 05,2013 10:09:00 AM
        /// </method>
        public SqlDataReader executeSelectQueryDtReader(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dt = new DataTable();
            SqlDataReader reader = null;
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                reader = myCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                //myCommand.Connection.Close();
            }
            return reader;
        }

        /// <method>
        /// Method to Return Data Table -- Added by Shankar.M January 05,2012 04:13:00 PM
        /// </method>
        public DataTable executeSelectQueryOnlySPDataTable(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dt = new DataTable();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(dt);
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _query + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");

                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dt;
        }


        /// <method>
        /// Insert Query
        /// </method>
        public bool executeInsertQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                //myCommand.ExecuteNonQuery();
                _userentity.status = myCommand.ExecuteNonQuery();
                if (_userentity.status > 0)
                {
                    _userentity.flag = true;
                }
                else
                {
                    _userentity.flag = false;
                }
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message, "Error Message - A179");

                //Console.Write("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return _userentity.flag;
        }

        /// <method>
        /// Insert Query WITH STORED PROCEDURE
        /// </method>
        public bool executeInsertQueryWithSP(String _storedprocedure, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _storedprocedure;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                _userentity.status = myCommand.ExecuteNonQuery();
                if (_userentity.status > 0)
                {
                    _userentity.flag = true;
                }
                else
                {
                    _userentity.flag = false;
                }
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _storedprocedure + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");
                //Console.Write("Error - Connection.executeInsertQuery - Query: " + _storedprocedure + " \nException: \n" + e.StackTrace.ToString());
                //return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return _userentity.flag;

        }

        /// <method>
        /// Insert Query WITH STORED PROCEDURE RETURNING DATASET
        /// </method>
        public DataSet executeInsertQueryDSWithSP(String _storedprocedure, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandTimeout = 0;
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _storedprocedure;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                //myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                //if (_userentity.status > 0)
                //{
                //    _userentity.flag = true;
                //}
                //else
                //{
                //    _userentity.flag = false;
                //}
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message + "\r\n" + "Procedure : " + _storedprocedure + "\r\n" + "Line : " + e.LineNumber.ToString(), "Error Message - A179");

                //MessageBox.Show(e.Message);
                //Console.Write("Error - Connection.executeInsertQuery - Query: " + _storedprocedure + " \nException: \n" + e.StackTrace.ToString());
                //return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return ds;

        }


        public DataTable executeSelectQueryBayandRack(String _query)
        {
            SqlCommand myCommand = new SqlCommand();
            DataTable dataTable = new DataTable();
            dataTable = null;
            DataSet ds = new DataSet();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                //  myCommand.Parameters.AddRange(sqlParameter);
                myCommand.ExecuteNonQuery();
                myAdapter.SelectCommand = myCommand;
                myAdapter.Fill(ds);
                dataTable = ds.Tables[0];
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeSelectQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return dataTable;
        }

        /// <method>
        /// Update Query
        /// </method>
        public bool executeUpdateQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.UpdateCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                //MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return true;
        }
        public bool executeUpdateQueryTest(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.UpdateCommand = myCommand;
                myCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                // MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                myCommand.Connection.Close();
            }
            return true;
        }
        public string Backlog_fich_generation(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            string retn = string.Empty;
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = _query;
                myCommand.Parameters.AddRange(sqlParameter);
                myAdapter.InsertCommand = myCommand;
                myCommand.ExecuteNonQuery();

                retn = Convert.ToString(myCommand.Parameters["@Return"].Value);
            }
            catch (SqlException e)
            {
                //  MessageBox.Show(e.Message, "Error Message - A179");
                //Console.Write("Error - Connection.executeUpdateQuery - Query: " + _query + " \nException: " + e.StackTrace.ToString());

            }
            finally
            {
                myCommand.Connection.Close();
            }
            return retn;
        }
    }

}
