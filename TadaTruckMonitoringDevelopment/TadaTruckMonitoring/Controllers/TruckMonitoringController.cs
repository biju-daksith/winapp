﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using TadaTruckMonitoring.Model;
using TadaTruckMonitoring.Models;
using System.IO;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
namespace TadaTruckMonitoring.Controllers
{
    public class TruckMonitoringController : Controller
    {

        private Dictionary<string, string> _jsonData;
        Truck_Dashboard_Model _usermodel = new Truck_Dashboard_Model();
        MessageValidationModel _alertmodel;
        MessageValidationEntity _alertentity = new MessageValidationEntity();
        public ActionResult TruckStatus()
        {
            return View();
        }
        [HttpGet]
        public ActionResult getDriverDetails(string Status)
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadDriverDetails(Status);
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            string jsonTBL01 = getjsondata(dt.Tables[1]);
            var _LstHolding = new { jsonTBL00, jsonTBL01 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult getOtherDetails()
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadOtherDetails();
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            string jsonTBL01 = getjsondata(dt.Tables[1]);
            var _LstHolding = new { jsonTBL00, jsonTBL01 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ExportPDF(string pdfpath, string GTrackNo)
        {

            string PDFPath = "";
            string PDFUserID = "";
            string PDFPassword = "";

            var ext_fileName = pdfpath;
            WebClient client = new WebClient();
            DataSet dt = new DataSet();
            dt = _usermodel.getFTPPDFDetails();
            if (dt.Tables[0].Rows.Count == 1)
            {
                foreach (DataRow row in dt.Tables[0].Rows)
                {
                    PDFPath = Convert.ToString(row["PATH"].ToString());
                    PDFUserID = Convert.ToString(row["USERID"].ToString());
                    PDFPassword = Convert.ToString(row["PASWORD"].ToString());
                }
            }

            if (PDFPath != "" && PDFUserID != "" && PDFPassword != "")
            {
                string url = string.Concat(PDFPath + pdfpath);
                client.Credentials = new NetworkCredential(PDFUserID, PDFPassword);
                if (CheckIfFileExistsOnServer(url, PDFUserID, PDFPassword) == true)
                {
                    byte[] contents = client.DownloadData(url);
                    if (contents.Length > 0)
                    {
                        //System.Text.Encoding enc = System.Text.Encoding.ASCII;
                        //string myString = //enc.GetString(contents);
                        string path = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
                        System.IO.File.WriteAllBytes(path + pdfpath, contents);
                    }
                    return Json(new { fileName = ext_fileName, gtrack = GTrackNo, errorMessage = "" });
                }
                else
                {
                    return Json(new { fileName = "", gtrack = GTrackNo, errorMessage = "" });
                }
            }
            else
            {
                return Json(new { fileName = "", gtrack = GTrackNo, errorMessage = "" });
            }
        }

        [HttpPost]
        public JsonResult ExportExcel(string trpath, string GTrackNo)
        {

            string FTPPath = "";
            string FTPUserID = "";
            string FTPPassword = "";

            var ext_fileName = trpath;
            WebClient client = new WebClient();
            DataSet dt = new DataSet();
            dt = _usermodel.getFTPDetails();
            if (dt.Tables[0].Rows.Count == 1)
            {
                foreach (DataRow row in dt.Tables[0].Rows)
                {
                    FTPPath = Convert.ToString(row["PATH"].ToString());
                    FTPUserID = Convert.ToString(row["USERID"].ToString());
                    FTPPassword = Convert.ToString(row["PASWORD"].ToString());
                }
            }

            if (FTPPath != "" && FTPUserID != "" && FTPPassword != "")
            {
                string url = string.Concat(FTPPath + trpath);
                client.Credentials = new NetworkCredential(FTPUserID, FTPPassword);
                if (CheckIfFileExistsOnServer(url, FTPUserID, FTPPassword) == true)
                {
                    byte[] contents = client.DownloadData(url);
                    if (contents.Length > 0)
                    {
                        System.Text.Encoding enc = System.Text.Encoding.ASCII;
                        string myString = enc.GetString(contents);
                        string path = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
                        System.IO.File.WriteAllText(path + trpath, myString);
                    }
                    return Json(new { fileName = ext_fileName, gtrack = GTrackNo, errorMessage = "" });
                }
                else
                {
                    return Json(new { fileName = "", gtrack = GTrackNo, errorMessage = "" });
                }
            }
            else
            {
                return Json(new { fileName = "", gtrack = GTrackNo, errorMessage = "" });
            }
        }
        private bool CheckIfFileExistsOnServer(string fileName, string UserID, string Password)
        {
            var request = (FtpWebRequest)WebRequest.Create(fileName);
            request.Credentials = new NetworkCredential(UserID, Password);
            request.Method = WebRequestMethods.Ftp.GetFileSize;
            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
            }
            return false;
        }


        [HttpGet]
        public ActionResult Download(string file, string gtrack)
        {
            string fullPath = Path.Combine(Server.MapPath("~/App_Data"), file);
            DataSet dt = new DataSet();
            dt = _usermodel.setcheckstauts("JSON", gtrack);
            return File(fullPath, "application/json", file);
        }



        [HttpGet]
        public ActionResult DownloadPDF(string file, string gtrack)
        {
            string fullPath = Path.Combine(Server.MapPath("~/App_Data"), file);
            DataSet dt = new DataSet();
            dt = _usermodel.setcheckstauts("PDF", gtrack);
            return File(fullPath, "application/pdf", file);
        }

        [HttpGet]
        public ActionResult Error_Message(string code)
        {
            _alertmodel = new MessageValidationModel();
            _alertentity = _alertmodel.Alert_Information("LG", code, "E");
            return Content(code + "---" + _alertentity.alertmessage);
        }

        [HttpGet]
        public ActionResult getCustomDetails()
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadCustomDetails();
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            string jsonTBL01 = getjsondata(dt.Tables[1]);
            var _LstHolding = new { jsonTBL00, jsonTBL01 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult updateEwayDetails(string GTrackNo, string InvoiceNo, string EWayBillNo, string TransitNo, string TruckOut)
        {
            string APIPath = "";
            string Status = "";
            DataSet dt = new DataSet();
            dt = _usermodel.getAPIDetails();
            if (dt.Tables[0].Rows.Count == 1)
            {
                foreach (DataRow row in dt.Tables[0].Rows)
                {
                    APIPath = Convert.ToString(row["PATH"].ToString());
                }
            }
            if (APIPath != "")
            {
                string apiUrl = APIPath;
                WebClient client = new WebClient();
                client.Headers["Content-type"] = "application/json";
                client.Encoding = Encoding.UTF8;
                string URL = Convert.ToString(apiUrl + "/" + GTrackNo + "/" + InvoiceNo + "/" + EWayBillNo + "/" + TransitNo+"/"+DateTime.Now.ToString("yyyyMMddHHmmss"));// + "/" + "20190401001258");
                string json = client.DownloadString(URL);
                dynamic d = JObject.Parse(json);
                Status = (string)d["status"];
            }            
            //dt = _usermodel.updateEwayDetails(GTrackNo, InvoiceNo, EWayBillNo, TransitNo);
            //string Status = Convert.ToString(dt.Tables[0].Rows[0]["Status"].ToString());
            return Json(Status, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SaveDetails(string ID, string Sta)
        {
            DataSet dt = new DataSet();
            dt = _usermodel.SaveDetails(ID, Sta);
            string Status = Convert.ToString(dt.Tables[0].Rows[0]["Status"].ToString());
            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        public string getjsondata(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> Rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Row = null;
            foreach (DataRow dr in dt.Rows)
            {
                Row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Row.Add(col.ColumnName.Trim(), dr[col]);
                }
                Rows.Add(Row);
            }
            return jsSerializer.Serialize(Rows);
        }

    }
}