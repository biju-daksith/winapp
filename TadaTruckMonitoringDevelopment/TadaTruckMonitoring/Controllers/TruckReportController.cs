﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using TadaTruckMonitoring.Model;
using TadaTruckMonitoring.Models;
using System.IO;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TadaTruckMonitoring.Controllers
{
    public class TruckReportController : Controller
    {
        // GET: TruckReport
        //public ActionResult Index()
        //{
        //    return View();
        //}

        private Dictionary<string, string> _jsonData;
        Truck_Report_Model _usermodel = new Truck_Report_Model();
        MessageValidationModel _alertmodel;
        MessageValidationEntity _alertentity = new MessageValidationEntity();

        public ActionResult TruckReport()
        {
            return View();
        }

        [HttpGet]
        public ActionResult getMattype(string lang)
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadMatDetails("E");
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            var _LstHolding = new { jsonTBL00};
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);

        }


        [HttpGet]
        public ActionResult GetTruckReport(string FromDate, string ToDate, string Mattype)
        {

            DataTable dt = new DataTable();
            dt = _usermodel.GetTruckReport(FromDate, ToDate, Mattype);
            string jsonTBL00 = getjsondata(dt);
            var _LstHolding = new { jsonTBL00 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);
        }



        private bool CheckIfFileExistsOnServer(string fileName, string UserID, string Password)
        {
            var request = (FtpWebRequest)WebRequest.Create(fileName);
            request.Credentials = new NetworkCredential(UserID, Password);
            request.Method = WebRequestMethods.Ftp.GetFileSize;
            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                    return false;
            }
            return false;
        }


        [HttpGet]
        public ActionResult Download(string file)
        {
            string fullPath = Path.Combine(Server.MapPath("~/App_Data"), file);
            return File(fullPath, "application/json", file);
        }



        [HttpGet]
        public ActionResult DownloadPDF(string file)
        {
            string fullPath = Path.Combine(Server.MapPath("~/App_Data"), file);
            return File(fullPath, "application/pdf", file);
        }

        [HttpGet]
        public ActionResult Error_Message(string code)
        {
            _alertmodel = new MessageValidationModel();
            _alertentity = _alertmodel.Alert_Information("LG", code, "E");
            return Content(code + "---" + _alertentity.alertmessage);
        }



       



        public string getjsondata(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> Rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Row = null;
            foreach (DataRow dr in dt.Rows)
            {
                Row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Row.Add(col.ColumnName.Trim(), dr[col]);
                }
                Rows.Add(Row);
            }
            return jsSerializer.Serialize(Rows);
        }


    }
}