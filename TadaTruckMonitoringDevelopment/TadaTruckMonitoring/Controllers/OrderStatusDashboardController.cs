﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using TadaTruckMonitoring.Model;
using TadaTruckMonitoring.Models;
using System.IO;
using System.Web.Script.Serialization;
using System.Configuration;

namespace TadaTruckMonitoring.Controllers
{
    public class OrderStatusDashboardController : Controller
    {
        //
        // GET: /TruckDashboard/

        Truck_Dashboard_Model _usermodel = new Truck_Dashboard_Model();
        public ActionResult OrderStatus() 
        {
            if (Session["LOGIN_STAT"] != "SUCCESS")
            {
                Session["GLOBAL_CONN"] = ConfigurationManager.ConnectionStrings["ADMINEURConnectionString"].ConnectionString;
                return this.RedirectToAction("Login", "Login");
            }
            return View();
        }


        [HttpGet]
        public ActionResult getDriverDetails(string Status)
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadDriverDetails(Status);
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            string jsonTBL01 = getjsondata(dt.Tables[1]);
            var _LstHolding = new { jsonTBL00, jsonTBL01 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);

        }

        [HttpGet]
        public ActionResult getCustomDetails()
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadCustomDetails();
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            string jsonTBL01 = getjsondata(dt.Tables[1]);
            var _LstHolding = new { jsonTBL00, jsonTBL01 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);
        }



        [HttpGet]
        public ActionResult getOtherDetails()
        {
            DataSet dt = new DataSet();
            dt = _usermodel.LoadOtherDetails();
            string jsonTBL00 = getjsondata(dt.Tables[0]);
            string jsonTBL01 = getjsondata(dt.Tables[1]);
            var _LstHolding = new { jsonTBL00, jsonTBL01 };
            return Json(_LstHolding, JsonRequestBehavior.AllowGet);
        }


        //public ActionResult SaveDetails(string tblStr)
        //{
        //    DataSet dt = new DataSet();
        //    dt = _usermodel.SaveDetails(tblStr);
        //    string Status = Convert.ToString(dt.Tables[0].Rows[0]["Status"].ToString());
        //    return Json(Status, JsonRequestBehavior.AllowGet);
        //}


        public string getjsondata(DataTable dt)
        {
            JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
            List<Dictionary<string, object>> Rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> Row = null;
            foreach (DataRow dr in dt.Rows)
            {
                Row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    Row.Add(col.ColumnName.Trim(), dr[col]);
                }
                Rows.Add(Row);
            }
            return jsSerializer.Serialize(Rows);
        }


    }
}