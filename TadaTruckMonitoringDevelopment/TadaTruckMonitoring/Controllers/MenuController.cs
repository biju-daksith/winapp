﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace TadaTruckMonitoring.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/
        public ActionResult Menu()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("<li class=\"\">");
            sb.Append(Environment.NewLine);
            sb.Append("<a href=\"\">");
            sb.Append("Internal Movements of ID Sheets");
            sb.Append("</a>");
            sb.Append(Environment.NewLine);
            sb.Append("</li>");

            //string return_data = "<li class=""><a href="">Internal Movements of ID Sheets</a></li>";
            return View(sb.ToString());
            //return View();
        }
	}
}