﻿using TadaTruckMonitoring.Models;
using TadaTruckMonitoring.Control;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TadaTruckMonitoring.Controllers
{
    [SessionTimeout]
    public class Error_MSGController : Controller
    {
        MessageValidationModel _alertmodel;
        MessageValidationEntity _alertentity = new MessageValidationEntity();

        //
        // GET: /Error_MSG/
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Error_Message(string code)
        {
            _alertmodel = new MessageValidationModel();
            _alertentity = _alertmodel.Alert_Information("LG", code, "E");
            return Content(code + "---" + _alertentity.alertmessage);
        }

    }
}