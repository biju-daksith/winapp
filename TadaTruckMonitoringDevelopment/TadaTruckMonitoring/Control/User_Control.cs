﻿using TadaTruckMonitoring.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TadaTruckMonitoring.Control
{


    public class User_Control
    {
        DBConnection conn = new DBConnection();


        ///<Method>
        /// Null Text Validation
        /// </Method>
        public DataTable getCurrLnag(string _lang)
        {
            string query = string.Format("SELECT top 1 C_LNGCODE FROM [XT_LANG] WHERE C_STA='Y' AND C_LANG=@lang");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@lang", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_lang);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        public DataTable getPasswordHistory(string username, string password)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(username);
                sqlParameters[1] = new SqlParameter("@PASSWORD", SqlDbType.VarChar);
                sqlParameters[1].Value = Convert.ToString(password);
                return conn.executeSelectQuerySPDataTable("SP_ADMIN_GETPWDH_LITE", sqlParameters);
            }
            catch
            {
                throw;
            }
        }

        /// <method>
        //Update the new password
        /// </method>
        public bool getUpdateNewPwd(string _username, string _newpwd)
        {
            //string query = string.Format("update [xt_xuti] set MOT_PAS=@password where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_sgid);
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_newpwd);
            return conn.executeInsertQueryWithSP("SP_ADMIN_CHANGEPASSWORD", sqlParameters);
        }


        /// <method>
        //Insert login user to table
        /// </method>
        public bool getDeleteLoginUsers(string _username, string _sgid)
        {
            //string query = string.Format("insert into XT_AVLUSR(nom_uti,nom_sgid,current_ip,login_date) values()");
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@FLAG", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("DELETE_LOGIN_USER_PROC");
            sqlParameters[1] = new SqlParameter("@SGID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_username);
            sqlParameters[2] = new SqlParameter("@PW", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_sgid);
            return conn.executeInsertQueryWithSP("SP_ADMIN_WEB_LOGIN", sqlParameters);
        }



        /// <method>
        //Validate the login users
        /// </method>
        public DataTable getValidateLoginUser(string _username, string _password)
        {
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS=@password");
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@FLAG", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("GET_XUTI_DETAILS");
            sqlParameters[1] = new SqlParameter("@SGID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_username);
            sqlParameters[2] = new SqlParameter("@PW", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_password);
            return conn.executeSelectQuerySPDataTable("SP_ADMIN_WEB_LOGIN", sqlParameters);
        }
        /// <method>
        //Get Data
        /// </method>
        public DataTable getMasterData(string _site, string _flag)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@SITE", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(_site);
                sqlParameters[1] = new SqlParameter("@FLAG", SqlDbType.VarChar);
                sqlParameters[1].Value = Convert.ToString(_flag);
                return conn.executeSelectQuerySPDataTable("SP_ADMIN_GETDCPDATA_LITE", sqlParameters);
            }
            catch
            {
                throw;
            }
        }
        /// <method>
        //Bind Form Controls Based on Language
        /// </method>
        public DataSet getLoadMainMenus(string _language, string _application, string _usergroup, string _menugroup, string _userid)
        {
            //string query = string.Format("select langname,langdesc,frmname,frmctrl,frmctrlid,frmctlname,displayorder from [XT_GCRITEM] where langname=@langname and frmname=@formname order by displayorder asc");
            SqlParameter[] sqlParameters = new SqlParameter[5];
            sqlParameters[0] = new SqlParameter("@LANGUAGE", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_language);
            sqlParameters[1] = new SqlParameter("@APPLICATION", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_application);
            sqlParameters[2] = new SqlParameter("@USERGROUP", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_usergroup);
            sqlParameters[3] = new SqlParameter("@MENUGROUP", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(_menugroup);
            sqlParameters[4] = new SqlParameter("@USERID", SqlDbType.VarChar);
            sqlParameters[4].Value = Convert.ToString(_userid);
            ////return conn.executeSelectQueryWithSP("LG_LOAD_MENUSUBMENU", sqlParameters);
            //return conn.executeSelectQueryWithSP("LG_LOAD_MENUSUBMENUNEW_FR", sqlParameters);
            return conn.executeSelectQueryWithSP("LG_LOAD_MENUSUBMENUNEW_LITE", sqlParameters);
        }


        /// <method>
        //Insert login user to table
        /// </method>
        public bool getInsertLoginUsers(string _username, string _sgid, string _machinename, string _appversion)
        {
            //string query = string.Format("insert into XT_AVLUSR(nom_uti,nom_sgid,current_ip,login_date) values()");
            SqlParameter[] sqlParameters = new SqlParameter[5];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@NOM_SGID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_sgid);
            //sqlParameters[2] = new SqlParameter("@SP_ID", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(_sgid);
            //sqlParameters[2] = new SqlParameter("@NO_OF_USERCOUNT", SqlDbType.VarChar);
            //sqlParameters[2].Value = Convert.ToString(1);
            sqlParameters[2] = new SqlParameter("@LOGIN_STATUS", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(1);
            sqlParameters[3] = new SqlParameter("@MACHINENAME", SqlDbType.VarChar);
            sqlParameters[3].Value = Convert.ToString(_machinename);
            sqlParameters[4] = new SqlParameter("@APPVERSION", SqlDbType.VarChar);
            sqlParameters[4].Value = Convert.ToString(_appversion);
            return conn.executeInsertQueryWithSP("SP_ADMIN_INSERTLOGINUSER_PROC_LITE", sqlParameters);
        }



        public DataTable getPasswordChangeTime(string username, string password)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@USERNAME", SqlDbType.VarChar);
                sqlParameters[0].Value = Convert.ToString(username);
                sqlParameters[1] = new SqlParameter("@PASSWORD", SqlDbType.VarChar);
                sqlParameters[1].Value = Convert.ToString(password);
                return conn.executeSelectQuerySPDataTable("SP_ADMIN_GETPASSWORDCT_LITE", sqlParameters);
            }
            catch
            {
                throw;
            }
        }


        public DataTable getMultiUserLoginLock()
        {
            SqlParameter[] sqlParameters = new SqlParameter[0];
            return conn.executeSelectQuery("SP_ADMIN_GETMLOGINLOCK_LITE", sqlParameters);
        }
        /// <method>
        //Validate the multi user login
        /// </method>
        public DataTable getValidateMultiUserLogin(string _username)
        {
            //string query = string.Format("select * from [XT_AVLUSR] where nom_uti=@username and nom_sgid=@sgid and login_status=1");
            string query = string.Format("select * from [XT_AVLUSR] where nom_uti=@username and login_status=1");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@sgid", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_sgid);
            return conn.executeSelectQuery(query, sqlParameters);
        }
        public DataTable getValidateLoginUserOnly(string _username, string _password)
        {
            string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username and MOT_PAS<>@password");
            //string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        public DataTable getValidateLoginUserOnly(string _username)
        {
            string query = string.Format("select * from [xt_xuti] where NOM_UTI=@username");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@username", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@password", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeSelectQuery(query, sqlParameters);
        }

        /// <method>
        //Update the wrong password count to table
        /// </method>
        public bool getUpdateWrongPwd(string _username, string _password)
        {
            //string query = string.Format("update [xt_xuti] set NO_OF_ATTEMPT= where NOM_UTI=@username and MOT_PAS<>@password");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@MOT_PAS", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeInsertQueryWithSP("SP_ADMIN_UPDATEWRONGPWD_COUNT_PROC", sqlParameters);
        }

        //Update the wrong password count to table
        /// </method>
        public bool getAccountLock(string _username)
        {
            string query = string.Format("update [xt_xuti] set LOGIN_STATUS=0 where NOM_UTI=@NOM_UTI");
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@NOM_UTI", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_username);
            //sqlParameters[1] = new SqlParameter("@MOT_PAS", SqlDbType.VarChar);
            //sqlParameters[1].Value = Convert.ToString(_password);
            return conn.executeInsertQuery(query, sqlParameters);
        }
    }
}