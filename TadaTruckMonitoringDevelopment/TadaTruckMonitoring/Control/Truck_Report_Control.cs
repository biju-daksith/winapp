﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using TadaTruckMonitoring.Controllers;
using TadaTruckMonitoring.Models;
using Newtonsoft.Json;


namespace TadaTruckMonitoring.Control
{

    public class Truck_Report_Control
    {
        private DBConnection conn;
        public Truck_Report_Control()
        {
            conn = new DBConnection();
        }
        public DataSet LoadMatDetails(string lang)
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@lang", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(lang);
            return conn.executeSelectQueryWithSP("SP_TADA_GETMATREPORT", sqlParameters);
        }


        public DataTable GetTruckReport(string FromDate, string ToDate, string Mattype)
        {


            //DateTime? dtFromDate = FromDate == "" ? null : (DateTime?)DateTime.ParseExact(FromDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //DateTime? dtToDate = ToDate == "" ? null : (DateTime?)DateTime.ParseExact(ToDate, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            SqlParameter[] sqlParameters = new SqlParameter[3];

            if (FromDate != null)
            {
                sqlParameters[0] = new SqlParameter("@STDATE", SqlDbType.DateTime);
                sqlParameters[0].Value = FromDate;
            }
            else
            {
                sqlParameters[0] = new SqlParameter("@STDATE", SqlDbType.DateTime);
                sqlParameters[0].Value = DBNull.Value;
            }

            if (ToDate != null)
            {
                sqlParameters[1] = new SqlParameter("@ENDDATE", SqlDbType.DateTime);
                sqlParameters[1].Value = ToDate;
            }
            else
            {
                sqlParameters[1] = new SqlParameter("@ENDDATE", SqlDbType.DateTime);
                sqlParameters[1].Value = DBNull.Value;
            }

            if (!string.IsNullOrEmpty(Mattype))
            {
                sqlParameters[2] = new SqlParameter("@MATTYPE", SqlDbType.VarChar);
                sqlParameters[2].Value = Convert.ToString(Mattype);
            }
            else
            {
                sqlParameters[2] = new SqlParameter("@MATTYPE", SqlDbType.VarChar);
                sqlParameters[2].Value = DBNull.Value;
            }
            
            return conn.executeSelectQuerySPDataTable("SP_TADA_GETTRUCKREPORT", sqlParameters);
        }


    }


}
