﻿using TadaTruckMonitoring.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace TadaTruckMonitoring.Control
{
    public class SessionTimeout : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAjaxRequest())
            {
                if (HttpContext.Current.Session["SGID"] == null || HttpContext.Current.Session["SGID"].ToString() == "")
                {
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.Result = new JsonResult { Data = "LogOut" };
                }
            }
            else
            {
                if (HttpContext.Current.Session["SGID"] == null || HttpContext.Current.Session["SGID"].ToString() == "")
                {
                    filterContext.Result = new RedirectResult("~/Login/Login");
                    return;
                }
            }
        }
    }
}