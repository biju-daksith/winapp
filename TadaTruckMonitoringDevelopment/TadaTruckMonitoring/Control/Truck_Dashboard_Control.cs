﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using TadaTruckMonitoring.Controllers;
using TadaTruckMonitoring.Models;
using Newtonsoft.Json;

namespace TadaTruckMonitoring.Control
{
    public class Truck_Dashboard_Control
    {
        private DBConnection conn;
        public Truck_Dashboard_Control()
        {
            conn = new DBConnection();
        }
        public DataSet LoadDriverDetails(string Status)
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(Status);
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }
        public DataSet setcheckstauts(string Status, string Gtrack)
        {
            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(Status);
            sqlParameters[1] = new SqlParameter("@REF_NO", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(Gtrack);
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }
        public DataSet LoadCustomDetails()
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("CUSTOM");
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }
        //public DataSet updateEwayDetails(string GTrackNo, string InvoiceNo, string EWayBillNo, string TransitNo)
        //{
        //    SqlParameter[] sqlParameters = new SqlParameter[5];
        //    sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
        //    sqlParameters[0].Value = Convert.ToString("UPDATE");
        //    sqlParameters[1] = new SqlParameter("@REF_NO", SqlDbType.VarChar);
        //    sqlParameters[1].Value = Convert.ToString(GTrackNo);
        //    sqlParameters[2] = new SqlParameter("@INV_NO", SqlDbType.VarChar);
        //    sqlParameters[2].Value = Convert.ToString(InvoiceNo);
        //    sqlParameters[3] = new SqlParameter("@EWAYBILL_NO", SqlDbType.VarChar);
        //    sqlParameters[3].Value = Convert.ToString(EWayBillNo);
        //    sqlParameters[4] = new SqlParameter("@TRANSIT_NO", SqlDbType.VarChar);
        //    sqlParameters[4].Value = Convert.ToString(TransitNo);
        //    return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        //}

        public DataSet LoadOtherDetails()
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("PLANT");
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }
        public DataSet getFTPDetails()
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("GETFTP");
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }

        public DataSet getAPIDetails()
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("GETAPI");
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }

        public DataSet getFTPPDFDetails()
        {
            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("GETFTPPDF");
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }
        public DataSet SaveDetails(string ID, string Status)
        {
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString("SAVE");
            sqlParameters[1] = new SqlParameter("@ID", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(ID);
            sqlParameters[2] = new SqlParameter("@STA", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(Status);
            return conn.executeSelectQueryWithSP("SP_TRUCK_MONITORING_IND", sqlParameters);
        }
    }
}