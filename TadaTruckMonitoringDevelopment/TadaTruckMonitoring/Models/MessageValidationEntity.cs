﻿using System;
using System.Web;

namespace TadaTruckMonitoring.Models
{
    public class MessageValidationEntity
    {
     
       private string _message;
       private string _alertcode = string.Empty;
       private string _alerttitle;

        
        /// <constructor>
        /// Constructor UserVO
        /// </constructor>
       public MessageValidationEntity()
        {
            //
            // TODO: Add constructor logic here
            //
        }

       public string alertmessage
       {
           get
           {
               return _message;
           }

           set
           {
               _message = value;
           }
       } 

       public string alerttitle
       {
           get
           {
               return _alerttitle;
           }

           set
           {
               _alerttitle = value;
           }
       }

    }
}
