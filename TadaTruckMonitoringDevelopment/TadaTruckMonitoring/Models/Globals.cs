﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Orion_Lite.Models
{
    public static class Globals
    {
        // parameterless constructor required for static class
        static Globals()
        {
            GlobalUsername = ""; GlobalLanguage = ""; GlobalSGID = ""; GlobalLineitem = "";
            Globalpo = ""; GlobalInwardno = ""; Globalficheno = ""; Globalreceived = ""; Globalproduct = "";
            GlobalOrginalSupCode = ""; GlobalCParam = ""; GlobalSite = ""; GlobalForm = ""; GlobalApplication = "";
            GlobalUsergroup = ""; GlobalConnection = ""; GlobalTransLanguage = ""; GlobalCurrTransLanguage = ""; GlobalFULLName = "";
        } // default value

        // public get, and private set for strict access control
        public static string GlobalUsername { get; private set; }
        public static string GlobalLanguage { get; private set; }
        public static string GlobalSGID { get; private set; }
        public static string GlobalLineitem { get; private set; }
        public static string Globalpo { get; private set; }
        public static string GlobalInwardno { get; private set; }
        public static string Globalficheno { get; private set; }
        public static string Globalreceived { get; private set; }
        public static string Globalproduct { get; private set; }
        public static string GlobalOrginalSupCode { get; private set; }
        public static string GlobalCParam { get; private set; }
        public static string GlobalSite { get; private set; }
        public static string GlobalForm { get; private set; }
        public static string GlobalApplication { get; private set; }
        public static string GlobalUsergroup { get; private set; }
        public static string GlobalConnection { get; private set; }
        public static string GlobalTransLanguage { get; private set; }
        public static string GlobalCurrTransLanguage { get; private set; }

        public static string GlobalFULLName { get; private set; }
        // GlobalInt can be changed only via this method
        public static void SetGlobalConnection(string connection)
        {
            GlobalConnection = connection;
        }
        // GlobalInt can be changed only via this method
        public static void SetGlobalString(string fullName, string newString, string language, string SGID, string cparam, string site, string form, string application, string usergroup, string tlang)
        {
            GlobalFULLName = fullName;
            GlobalUsername = newString;
            GlobalLanguage = language;
            GlobalSGID = SGID;
            GlobalCParam = cparam;
            GlobalSite = site;
            GlobalForm = form;
            GlobalApplication = application;
            GlobalUsergroup = usergroup;
            GlobalTransLanguage = tlang;
        }
        // GlobalInt can be changed only via this method
        public static void SetGlobalOriginalSuplierCodeString(string supcode, string product)
        {
            GlobalOrginalSupCode = supcode;
            Globalproduct = product;
        }
        // GlobalInt can be changed only via this method
        public static void SetInwardGlobalString(string lineitems, string po, string inwardno, string product)
        {
            GlobalLineitem = lineitems;
            Globalpo = po;
            GlobalInwardno = inwardno;
            Globalproduct = product;
        }

        // GlobalInt can be changed only via this method
        public static void SetInwardChildGlobalString(string lineitems, string ficheno, string inwardno, string pono)
        {
            GlobalLineitem = lineitems;
            Globalficheno = ficheno;
            GlobalInwardno = inwardno;
            Globalpo = pono;
        }

        public static void SetGlobalLangString(string currtlang)
        {
            GlobalCurrTransLanguage = currtlang;
        }
    }
}