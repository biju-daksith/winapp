﻿using System;
using System.Data;
using System.Web;

namespace TadaTruckMonitoring.Models
{
    public class MessageValidationModel
    {
        private MessageValidationControl _usercontrol = new MessageValidationControl();

        public MessageValidationModel()
        {
           // _usercontrol = new MessageValidationControl();
        }

        public MessageValidationEntity Alert_Information(string _app, string _err, string _lang)
       {

           MessageValidationEntity userVO = new MessageValidationEntity();
           DataTable dataTable = new DataTable();
           dataTable = _usercontrol.Alert_Info(_app, _err, _lang);

           foreach (DataRow dr in dataTable.Rows)
           { 
               userVO.alertmessage = dr["L_ERR"].ToString();
               userVO.alerttitle = dr["C_MSGTYPE"].ToString() + " - " + dr["C_ERR"].ToString();
           }
           return userVO;
       }

    }
}
