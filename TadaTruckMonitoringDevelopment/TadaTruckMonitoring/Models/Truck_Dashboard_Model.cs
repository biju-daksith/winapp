﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TadaTruckMonitoring.Model;
using TadaTruckMonitoring.Entity;
using TadaTruckMonitoring.Control;
using System.Data;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
namespace TadaTruckMonitoring.Models
{
    public class Truck_Dashboard_Model
    {

        private Truck_Dashboard_Control _usercontrol;
        public Truck_Dashboard_Model()
        {
            _usercontrol = new Truck_Dashboard_Control();
        }

        public DataSet LoadDriverDetails(string Status)
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.LoadDriverDetails(Status);
            return dataTable;
        }
        public DataSet setcheckstauts(string Status, string Gtrack)
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.setcheckstauts(Status, Gtrack);
            return dataTable;
        }
        
        public DataSet LoadCustomDetails()
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.LoadCustomDetails();
            return dataTable;
        }

        //public DataSet updateEwayDetails(string GTrackNo, string InvoiceNo, string EWayBillNo, string TransitNo)
        //{
        //    DataSet dataTable = new DataSet();
        //    dataTable = _usercontrol.updateEwayDetails(GTrackNo, InvoiceNo, EWayBillNo, TransitNo);
        //    return dataTable;
        //}

        public DataSet LoadOtherDetails()
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.LoadOtherDetails();
            return dataTable;
        }

        public DataSet getFTPDetails()
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.getFTPDetails();
            return dataTable;
        }

        public DataSet getAPIDetails() 
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.getAPIDetails();
            return dataTable;
        }

        public DataSet getFTPPDFDetails()
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.getFTPPDFDetails();
            return dataTable;
        }


        public DataSet SaveDetails(string ID, string Status)
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.SaveDetails(ID, Status);
            return dataTable;
        }

    }
}