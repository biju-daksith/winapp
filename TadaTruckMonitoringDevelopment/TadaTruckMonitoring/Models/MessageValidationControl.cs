﻿using TadaTruckMonitoring.Controllers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TadaTruckMonitoring.Models
{
    public class MessageValidationControl
    {
           // GET: /MessageValidationControl/
        private DBConnection conn = new DBConnection();

        public MessageValidationControl()
        {
           // conn = new DBConnection();
        }
        

        public DataTable Alert_Info(string _app, string _err, string _lang)
        {


            string query = string.Format("SELECT C_ERR,L_ERR,C_MSGTYPE FROM MA_XERRL WHERE  C_APP=@app AND C_ERR=@err AND C_LANG='E'");
            SqlParameter[] sqlParameters = new SqlParameter[3];
            sqlParameters[0] = new SqlParameter("@app", SqlDbType.VarChar);
            sqlParameters[0].Value = Convert.ToString(_app);
            sqlParameters[1] = new SqlParameter("@err", SqlDbType.VarChar);
            sqlParameters[1].Value = Convert.ToString(_err);
            sqlParameters[2] = new SqlParameter("@lang", SqlDbType.VarChar);
            sqlParameters[2].Value = Convert.ToString(_lang);
            return conn.executeSelectQuery(query, sqlParameters);
        }

    }
}