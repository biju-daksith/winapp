﻿using TadaTruckMonitoring.Control;
using TadaTruckMonitoring.Entity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace TadaTruckMonitoring.Models
{
    public class User_Model
    {
        User_Control _userControl = new User_Control();
        public bool status;
        public User_Entity getValidateLoginUserOnly(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserOnly(username, password);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
            }
            return userEntity;
        }

        public DataTable getPasswordHistory(string username, string password)
        {
            try
            {
                return _userControl.getPasswordHistory(username, password);
            }
            catch
            {
                throw;
            }
        }
        //Update the new password
        public bool getUpdateNewPwd(string username, string newpwd)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            userEntity.flag = _userControl.getUpdateNewPwd(username, newpwd);

            return userEntity.flag;
        }



        //Delete login users to table
        public bool getDeleteLoginUsers(string username, string sgid)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            status = _userControl.getDeleteLoginUsers(username, sgid);

            return status;
        }


        //Validate the multi user login
        public User_Entity getValidateMultiUserLogin(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateMultiUserLogin(username);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                //userEntity.noofusers = Convert.ToInt32(dr["no_of_usercount"]);
                userEntity.processid = dr["SP_ID"].ToString();
                userEntity.machinename = dr["MACHINE_NAME"].ToString();
                userEntity.sgid = dr["NOM_SGID"].ToString();
            }
            return userEntity;
        }

        public DataTable getMultiUserLoginLock()
        {
            DataTable dataTable = new DataTable();
            return dataTable = _userControl.getMultiUserLoginLock();
        }


        public string getCurrLnag(string lang)
        {

            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();
            dataTable = _userControl.getCurrLnag(lang);

            foreach (DataRow dr in dataTable.Rows)
            {
                //userEntity.currlang = dr["C_LNGCODE"].ToString();
                lang = dr["C_LNGCODE"].ToString();
            }
            return lang;
        }

        public DataTable getMasterData(string _site, string _flag)
        {
            try
            {
                return _userControl.getMasterData(_site, _flag);
            }
            catch
            {
                throw;
            }
        }

        //Bind controls based on language
        public DataSet getLoadMainMenus(string language, string application, string usergroup, string menugroup, string userid)
        {
            //User_Entity userEntity = new User_Entity();
            DataSet dataset = new DataSet();

            dataset = _userControl.getLoadMainMenus(language, application, usergroup, menugroup, userid);

            //foreach (DataRow dr in dataset.Tables[0].Rows)
            //{
            //    userEntity.formname = dr["frmname"].ToString();
            //    userEntity.formcontrol = dr["frmctrl"].ToString();
            //    userEntity.formcontrolid = dr["frmctrlid"].ToString();
            //    userEntity.formcontrolname = dr["frmctlname"].ToString();
            //    userEntity.language = dr["langname"].ToString();
            //}
            return dataset;
        }


        //Insert login users to table
        public bool getInsertLoginUsers(string username, string sgid, string machinename, string appversion)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            status = _userControl.getInsertLoginUsers(username, sgid, machinename, appversion);

            return status;
        }

       
        
        //Validate the default password time limit
        public DataTable getPasswordChangeTime(string username, string password)
        {
            try
            {
                return _userControl.getPasswordChangeTime(username, password);
            }
            catch
            {
                throw;
            }
        }
        //Validate the login users
        public User_Entity getValidateLoginUser(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUser(username, password);
            userEntity.loginstatus = 2;
            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.fullName = dr["FULLNAME"].ToString();
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
                userEntity.cparam = dr["C_PARM"].ToString();
                userEntity.sgid = dr["NOM_SGID"].ToString();
                userEntity.site = dr["C_LIEU"].ToString();
                userEntity.form = dr["TY_OUTIL"].ToString();
                if (dr["LOGIN_STATUS"] != null || dr["LOGIN_STATUS"].ToString() != "")
                {
                    userEntity.loginstatus = Convert.ToInt32(dr["LOGIN_STATUS"].ToString());
                }
                else
                {
                    userEntity.loginstatus = 1;
                }
            }
            return userEntity;
        }

        //Validate the login users
        public User_Entity getValidateLoginUserOnly(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            dataTable = _userControl.getValidateLoginUserOnly(username);

            foreach (DataRow dr in dataTable.Rows)
            {
                userEntity.username = dr["NOM_UTI"].ToString();
                userEntity.group = dr["C_UTI"].ToString();
                userEntity.term = dr["C_TERM"].ToString();
                userEntity.application = dr["C_APP"].ToString();
                userEntity.wrngpasslimit = Convert.ToInt32(dr["WRONG_PASS_LIMIT"]);
                userEntity.wrngpassattmpt = Convert.ToInt32(dr["NO_OF_ATTEMPT"]);
            }
            return userEntity;
        }

        //Update the wrong password count to table
        public bool getUpdateWrongPwd(string username, string password)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            userEntity.flag = _userControl.getUpdateWrongPwd(username, password);

            //foreach (DataRow dr in dataTable.Rows)
            //{
            //    userEntity.username = dr["NOM_UTI"].ToString();
            //    userEntity.group = dr["C_UTI"].ToString();
            //    userEntity.term = dr["C_TERM"].ToString();
            //    userEntity.application = dr["C_APP"].ToString();
            //}
            return userEntity.flag;
        }

        //Update the wrong password count to table
        public bool getAccountLock(string username)
        {
            User_Entity userEntity = new User_Entity();
            DataTable dataTable = new DataTable();

            userEntity.flag = _userControl.getAccountLock(username);

            //foreach (DataRow dr in dataTable.Rows)
            //{
            //    userEntity.username = dr["NOM_UTI"].ToString();
            //    userEntity.group = dr["C_UTI"].ToString();
            //    userEntity.term = dr["C_TERM"].ToString();
            //    userEntity.application = dr["C_APP"].ToString();
            //}
            return userEntity.flag;
        }


    }


}