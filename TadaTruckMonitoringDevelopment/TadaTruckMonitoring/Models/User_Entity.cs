﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TadaTruckMonitoring.Entity
{
    public class User_Entity
    {
        private string _username, _group, _statusstr, _currlang, _fullName;
        private string _application, _term, _cparam, _site, _form, _connection, _machinename, _processid;
        private string _formname, _formcontrol, _formcontrolid, _formcontrolname, _language, _sgid, _languagevalue;
        private int _wrngpasslimit, _wrngpassattmpt;
        private int _noofusers,_status,_langid;
        private bool _flag;
        private int _loginstatus;
        /// <constructor>
        /// Constructor UserVO
        /// </constructor>
        public User_Entity()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public string fullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                _fullName = value;
            }
        }
        
        public string username
        {
            get
            {
                return _username;
            }

            set
            {
                _username = value;
            }
        }

        public string currlang
        {
            get
            {
                return _currlang;
            }

            set
            {
                _currlang = value;
            }
        }

        public string machinename
        {
            get
            {
                return _machinename;
            }

            set
            {
                _machinename = value;
            }
        }

        public string processid
        {
            get
            {
                return _processid;
            }

            set
            {
                _processid = value;
            }
        }

        public string statusstr
        {
            get
            {
                return _statusstr;
            }

            set
            {
                _statusstr = value;
            }
        }

        public string group
        {
            get
            {
                return _group;
            }
            set
            {
                _group = value;
            }
        }

        public string application
        {
            get
            {
                return _application;
            }

            set
            {
                _application = value;
            }
        }
        public string term
        {
            get
            {
                return _term;
            }

            set
            {
                _term = value;
            }
        }

        public string formname
        {
            get
            {
                return _formname;
            }

            set
            {
                _formname = value;
            }
        }

        public string language
        {
            get
            {
                return _language;
            }

            set
            {
                _language = value;
            }
        }

        public string connection
        {
            get
            {
                return _connection;
            }

            set
            {
                _connection = value;
            }
        }

        public string languagevalue
        {
            get
            {
                return _languagevalue;
            }

            set
            {
                _languagevalue = value;
            }
        }

        public string formcontrol
        {
            get
            {
                return _formcontrol;
            }

            set
            {
                _formcontrol = value;
            }
        }

        public string formcontrolid
        {
            get
            {
                return _formcontrolid;
            }

            set
            {
                _formcontrolid = value;
            }
        }
        
        public string formcontrolname
        {
            get
            {
                return _formcontrolname;
            }

            set
            {
                _formcontrolname = value;
            }
        }
        
        public string sgid
        {
            get
            {
                return _sgid;
            }

            set
            {
                _sgid = value;
            }
        }
        public string cparam
        {
            get
            {
                return _cparam;
            }

            set
            {
                _cparam = value;
            }
        }
        public string site
        {
            get
            {
                return _site;
            }

            set
            {
                _site = value;
            }
        }
        public string form
        {
            get
            {
                return _form;
            }

            set
            {
                _form = value;
            }
        }


        
        public int status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
            }
        }
        public int loginstatus
        {
            get
            {
                return _loginstatus;
            }

            set
            {
                _loginstatus = value;
            }
        }
        public int langid
        {
            get
            {
                return _langid;
            }

            set
            {
                _langid = value;
            }
        }

        public int wrngpasslimit
        {
            get
            {
                return _wrngpasslimit;
            }

            set
            {
                _wrngpasslimit = value;
            }
        }
        public int wrngpassattmpt
        {
            get
            {
                return _wrngpassattmpt;
            }

            set
            {
                _wrngpassattmpt = value;
            }
        }
        public int noofusers
        {
            get
            {
                return _noofusers;
            }

            set
            {
                _noofusers = value;
            }
        }

        public bool flag
        {
            get
            {
                return _flag;
            }

            set
            {
                _flag = value;
            }
        }
}
    }
