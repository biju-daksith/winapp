﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TadaTruckMonitoring.Model;
using TadaTruckMonitoring.Entity;
using TadaTruckMonitoring.Control;
using System.Data;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;


namespace TadaTruckMonitoring.Models
{

    public class Truck_Report_Model
    {

        private Truck_Report_Control _usercontrol;
        public Truck_Report_Model()
        {
            _usercontrol = new Truck_Report_Control();
        }

        public DataSet LoadMatDetails(string lang)
        {
            DataSet dataTable = new DataSet();
            dataTable = _usercontrol.LoadMatDetails(lang);
            return dataTable;
        }


        public DataTable GetTruckReport(string FromDate, string ToDate, string Mattype)
        {
            DataTable dt = new DataTable();
            dt = _usercontrol.GetTruckReport(FromDate, ToDate, Mattype);
            return dt;
        }

    }
}